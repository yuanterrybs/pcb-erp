package com.ruoyi.erp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 分类型号属性关联对象 erp_classify_mode_attribute_rt
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpClassifyModeAttributeRt extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 产品类型 */
    @Excel(name = "产品类型")
    private String modeType;

    /** 属性ID */
    @Excel(name = "属性ID")
    private String attributeId;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 启用状态 */
    @Excel(name = "启用状态")
    private String useState;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setModeType(String modeType) 
    {
        this.modeType = modeType;
    }

    public String getModeType() 
    {
        return modeType;
    }
    public void setAttributeId(String attributeId) 
    {
        this.attributeId = attributeId;
    }

    public String getAttributeId() 
    {
        return attributeId;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setUseState(String useState) 
    {
        this.useState = useState;
    }

    public String getUseState() 
    {
        return useState;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("modeType", getModeType())
            .append("attributeId", getAttributeId())
            .append("sort", getSort())
            .append("useState", getUseState())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
