package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpBatchInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpConsigneeInfo;
import com.ruoyi.erp.service.IErpConsigneeInfoService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 常用收货地址Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpConsigneeInfo")
public class ErpConsigneeInfoController extends BaseController
{
    private String prefix = "erp/erpConsigneeInfo";

    @Autowired
    private IErpConsigneeInfoService erpConsigneeInfoService;

    @RequiresPermissions("erp:erpConsigneeInfo:view")
    @GetMapping()
    public String erpConsigneeInfo()
    {
        return prefix + "/erpConsigneeInfo";
    }

    /**
     * 查询常用收货地址列表
     */
    @RequiresPermissions("erp:erpConsigneeInfo:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpConsigneeInfo erpConsigneeInfo)
    {
        startPage();
        List<ErpConsigneeInfo> list = erpConsigneeInfoService.selectErpConsigneeInfoList(erpConsigneeInfo);
        return getDataTable(list);
    }

    /**
     * 导出常用收货地址列表
     */
    @RequiresPermissions("erp:erpConsigneeInfo:export")
    @Log(title = "常用收货地址", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpConsigneeInfo erpConsigneeInfo)
    {
        List<ErpConsigneeInfo> list = erpConsigneeInfoService.selectErpConsigneeInfoList(erpConsigneeInfo);
        ExcelUtil<ErpConsigneeInfo> util = new ExcelUtil<ErpConsigneeInfo>(ErpConsigneeInfo.class);
        return util.exportExcel(list, "erpConsigneeInfo");
    }

    /**
     * 新增常用收货地址
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存常用收货地址
     */
    @RequiresPermissions("erp:erpConsigneeInfo:add")
    @Log(title = "常用收货地址", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpConsigneeInfo erpConsigneeInfo)
    {
        return toAjax(erpConsigneeInfoService.insertErpConsigneeInfo(erpConsigneeInfo));
    }

    /**
     * 修改常用收货地址
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpConsigneeInfo erpConsigneeInfo = erpConsigneeInfoService.selectErpConsigneeInfoById(id);
        mmap.put("erpConsigneeInfo", erpConsigneeInfo);
        return prefix + "/edit";
    }

    /**
     * 修改保存常用收货地址
     */
    @RequiresPermissions("erp:erpConsigneeInfo:edit")
    @Log(title = "常用收货地址", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpConsigneeInfo erpConsigneeInfo)
    {
        return toAjax(erpConsigneeInfoService.updateErpConsigneeInfo(erpConsigneeInfo));
    }

    /**
     * 删除常用收货地址
     */
    @RequiresPermissions("erp:erpConsigneeInfo:remove")
    @Log(title = "常用收货地址", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpConsigneeInfoService.deleteErpConsigneeInfoByIds(ids));
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpConsigneeInfo erpConsigneeInfo = erpConsigneeInfoService.selectErpConsigneeInfoById(id);
        mmap.put("erpConsigneeInfo", erpConsigneeInfo);
        return prefix + "/detail";
    }

    @RequiresPermissions("erp:erpConsigneeInfo:view")
    @GetMapping("/pageSelectConsignee")
    public String pageSelectConsignee() {
        return prefix + "/pageSelectConsignee";
    }
}
